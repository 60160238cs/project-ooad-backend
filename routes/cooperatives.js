const express = require('express')
const router = express.Router()

const cooperativeController = require('../controller/cooperativeController')

/* GET users listing. */
router.get('/', cooperativeController.getUsers)

router.get('/:id', cooperativeController.getUser)

router.post('/', cooperativeController.addUser)

router.put('/', cooperativeController.updateUser)

//อันเดียวใช้ได้กับ update ทั้งหมด
router.put('/editCooperative', cooperativeController.updateCooperative)

router.delete('/:id', cooperativeController.deleteUser)

// Route of lists

router.get('/list/:id', cooperativeController.getUserInList)

router.get('/coop/:id', cooperativeController.getCoop)

router.post('/list/:id', cooperativeController.addUserInList)

router.put('/list/:id', cooperativeController.updateUserInList)

router.delete('/list/:id', cooperativeController.deleteUserInList)

module.exports = router
