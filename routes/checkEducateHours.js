var express = require('express')
var router = express.Router()
const studentsController = require('../controller/StudentsController')

/* GET users listing. */
router.get('/', studentsController.getStudents)

router.get('/:id', studentsController.getStudent)

router.post('/', studentsController.addStudent)

router.put('/', studentsController.updateStudent)

router.delete('/:id', studentsController.deleteStudent)

module.exports = router
