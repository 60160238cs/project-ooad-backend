"use strict";

var express = require('express');

var router = express.Router();

var companiesController = require('../controller/CompaniesController');
/* GET users listing. */


router.get('/', companiesController.getCompanies);
router.get('/:id', companiesController.getCompany);
router.post('/', companiesController.addCompany);
router.put('/', companiesController.updateCompany);
router["delete"]('/:id', companiesController.deleteCompany);
module.exports = router;