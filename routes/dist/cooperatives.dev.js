"use strict";

var express = require('express');

var router = express.Router();

var cooperativeController = require('../controller/cooperativeController');
/* GET users listing. */


router.get('/', cooperativeController.getUsers);
router.get('/:id', cooperativeController.getUser);
router.post('/', cooperativeController.addUser);
router.put('/', cooperativeController.updateUser);
router["delete"]('/:id', cooperativeController.deleteUser); // Route of lists

router.get('/list/:id', cooperativeController.getUserInList);
router.get('/coop/:id', cooperativeController.getCoop);
router.post('/list/:id', cooperativeController.addUserInList);
router.put('/list/:id', cooperativeController.updateUserInList);
router["delete"]('/list/:id', cooperativeController.deleteUserInList);
module.exports = router;