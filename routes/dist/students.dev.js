"use strict";

var express = require('express');

var router = express.Router();

var studentController = require('../controller/StudentsController');
/* GET users listing. */


router.get('/', studentController.getStudents);
router.get('/id=:id', studentController.getStudentId);
router.get('/:id', studentController.getStudent);
router.post('/', studentController.addStudent);
router.put('/', studentController.updateStudent);
router["delete"]('/:id', studentController.deleteStudent);
module.exports = router;