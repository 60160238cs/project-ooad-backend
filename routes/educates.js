const express = require('express')
const router = express.Router()

const educateController = require('../controller/educateController')

/* GET users listing. */
router.get('/', educateController.getUsers)

router.get('/:id', educateController.getUser)

router.post('/', educateController.addUser)

router.put('/', educateController.updateUser)

//อันเดียวใช้ได้กับ update ทั้งหมด
router.put('/editEducate', educateController.updateEducate)

router.delete('/:id', educateController.deleteUser)

// Route of lists

router.post('/list/:id', educateController.addUserInList)

router.put('/list/:id', educateController.updateUserInList)

router.delete('/list/:id', educateController.deleteUserInList)

module.exports = router
