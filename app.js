var createError = require('http-errors')
var cors = require('cors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var companiesRouter = require('./routes/companies')
var educateRouter = require('./routes/educates')
var cooperativeRouter = require('./routes/cooperatives')
var checkCourseRouter = require('./routes/checkCourse')
var checkEducateHoursRouter = require('./routes/checkEducateHours')
var docsRouter = require('./routes/docs')
var showdocsRouter = require('./routes/showdocs')
var studentsRouter = require('./routes/students')

var mongoose = require('mongoose')
mongoose.connect(
  'mongodb+srv://admin_kub:admin_eng@mydb-u9ekq.gcp.mongodb.net/mydb?retryWrites=true&w=majority',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
)

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
try {
  db.once('open', err => {
    if (err) {
      console.error(err)
    }
    console.log('connected')
  })
} catch (e) {
  console.error(e)
}

var app = express()

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'jade')

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/companies', companiesRouter)
app.use('/educates', educateRouter)
app.use('/cooperatives', cooperativeRouter)
app.use('/checkCourse', checkCourseRouter)
app.use('/checkEducateHours', checkEducateHoursRouter)
app.use('/docs', docsRouter)
app.use('/showdocs', showdocsRouter)
app.use('/student', studentsRouter)
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
})

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message
  res.locals.error = req.app.get('env') === 'development' ? err : {}

  // render the error page
  res.status(err.status || 500)
  res.render('error')
})

module.exports = app
