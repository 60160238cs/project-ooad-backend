const mongoose = require('mongoose')
const Schema = mongoose.Schema
const studentSchema = new Schema({
  studentId: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 8
  },
  type: String,
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  faculty: {
    type: String
    // required: true
  },
  branch: {
    type: String,
    enum: ['วิทยาการคอมพิวเตอร์', 'สาขาวิชาเทคโนโลยีสารสนเทศ', 'สาขาวิชาวิศวกรรมซอฟต์แวร์']
  },
  gpax: {
    type: Number,
    required: true,
    min: 0,
    max: 4
  },
  cooperativeStatus: {
    type: Boolean,
    required: true
  },
  advisor: {
    type: String,
    required: true
  },
  company: {
    type: String
  },
  role: {
    type: String
  },
  educateTime: {
    academicTime: Number, // ชั่วโมงอบรม วิชาการ
    prepareTime: Number // ชั่วโมงอบรม เตรียมความพร้อม
  },
  registCooperativeStatus: Boolean, // สมัครสหกิจไปรึยัง?
  subject: [
    {
      subjectId: String,
      name: String,
      grade: Number
    }
  ]
})

module.exports = mongoose.model('Student', studentSchema)
