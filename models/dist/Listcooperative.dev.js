'use strict'

var mongoose = require('mongoose')

var Schema = mongoose.Schema
var listSchema = new Schema({
  cooperative: String,
  keyId: String,
  name: String,
  status: Boolean
})
module.exports = mongoose.model('Listcooperative', listSchema)
