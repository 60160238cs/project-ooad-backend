"use strict";

var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var studentSchema = new Schema({
  studentId: String,
  // รหัสนิสิต
  password: String,
  // รหัสผ่าน
  name: String,
  // ชื่อ
  faculty: String,
  // คณะ
  branch: String,
  // สาขา
  gpax: Number,
  // เกรดเฉลี่ย
  cooperativeStatus: Boolean,
  // สถานะสหกิจว่ากำลังฝึกงานอยู่
  advisor: String,
  // อาจารย์ที่ปรึกษา
  company: String,
  // บริษัท (ที่เข้าศึกษาสหกิจ)
  role: String,
  // ตำแหน่ง (ที่เข้าศึกษาสหกิจ)
  educateTime: {
    academicTime: Number,
    // ชั่วโมงอบรม วิชาการ
    prepareTime: Number // ชั่วโมงอบรม เตรียมความพร้อม

  },
  registCooperativeStatus: Boolean,
  // สมัครสหกิจไปรึยัง?
  subject: [{
    subjectId: String,
    name: String,
    grade: Number
  }],
  type: {
    type: String,
    "default": 'student'
  }
});
module.exports = mongoose.model('Student', studentSchema);