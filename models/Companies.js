const mongoose = require('mongoose')
const Schema = mongoose.Schema
const companySchema = new Schema({
  name: { type: String, required: true, minlength: 2 },
  phone: { type: String, required: true, minlength: 10, maxlength: 10 },
  email: { type: String, required: true },
  address: { type: String, required: true }
})

module.exports = mongoose.model('Company', companySchema)
