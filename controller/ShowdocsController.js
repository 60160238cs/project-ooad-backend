const Doc = require('../models/Doc')
const showdocsController = {
  documentList: [
    {
      ลำดับ: 1,
      ชื่อเอกสาร: 'ใบสมัครเป็นนิสิตสหกิจศึกษา',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S001.pdf'
    },
    {
      ลำดับ: 2,
      ชื่อเอกสาร: 'ใบสมัครงานสหกิจศึกษา/ฝึกงาน',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S002.pdf'
    },
    {
      ลำดับ: 3,
      ชื่อเอกสาร: 'แบบอนุญาตให้นิสิตไปปฏิบัติงานสหกิจศึกษา',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S003.pdf'
    }
  ],
  lastId: 4,

  async getDocs (req, res, next) {
    try {
      const docs = await Doc.find({})
      res.json(docs)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDoc (req, res, next) {
    const { id } = req.params
    // res.json(usersController.getUser(id))
    // User.findById(id)
    //   .then(user => {
    //     res.json(user)
    //   })
    //   .catch(err => {
    //     res.status(500).send(err)
    //   })
    try {
      const doc = await Doc.findById(id)
      res.json(doc)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = showdocsController
