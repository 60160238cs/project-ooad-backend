const Company = require('../models/Companies')
const companyController = {
  companyList: [
    {
      id: 1,
      name: 'ธนาคารไทยพาณิชย์ (SCB)',
      phone: '0277775555',
      email: 'scbprime@scb.co.th',
      address:
        'ธนาคารไทยพาณิชย์ จำกัด (มหาชน) สำนักงานใหญ่ 9 ถ.รัชดาภิเษก เขตจตุจักร กรุงเทพฯ 10900'
    },
    {
      id: 2,
      name: 'บริษัท ไมโครซอฟท์ (ประเทศไทย) จำกัด',
      phone: '0225749999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 3,
      name: 'กูเกิล Google Thailand (@GoogleThailand)',
      phone: '022574999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 4,
      name: 'Apple (Thailand)',
      phone: '0225749999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    },
    {
      id: 5,
      name: 'ซัมซุง ประเทศไทย (บ.ไทยซัมซุงอิเลคโทรนิคส์ จก.)',
      phone: '0225749999',
      email: 'scbprime@scb.co.th',
      address:
        'ชั้น 37 ยูนิต 1-7 ตึก CRC Tower ออลซีซันส์เพลส 87/2 ถนนวิทยุ แขวงลุมพินี เขตปทุมวัน กรุงเทพฯ 10330 ประเทศไทย'
    }
  ],
  lastId: 6,
  selectCompany: '',
  async addCompany (req, res, next) {
    const payload = req.body
    const company = new Company(payload)
    try {
      await company.save()
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateCompany (req, res, next) {
    const payload = req.body
    try {
      const company = await Company.updateOne({ _id: payload._id }, payload)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.deleteOne({ _id: id })
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompanies (req, res, next) {
    try {
      const companies = await Company.find({})
      res.json(companies)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getCompany (req, res, next) {
    const { id } = req.params
    try {
      const company = await Company.findById(id)
      res.json(company)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = companyController
