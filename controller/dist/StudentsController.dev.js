"use strict";

var Student = require('../models/Students');

var studentController = {
  studentList: [],
  lastId: 4,
  addStudent: function addStudent(req, res, next) {
    var payload, student;
    return regeneratorRuntime.async(function addStudent$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            payload = req.body; // res.json(studentController.addStudent(payload))

            student = new Student(payload);
            _context.prev = 2;
            _context.next = 5;
            return regeneratorRuntime.awrap(student.save());

          case 5:
            res.json(student);
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](2);
            res.status(500).send(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[2, 8]]);
  },
  updateStudent: function updateStudent(req, res, next) {
    var payload, student;
    return regeneratorRuntime.async(function updateStudent$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            payload = req.body; // res.json(studentController.updateStudent(payload))

            _context2.prev = 1;
            _context2.next = 4;
            return regeneratorRuntime.awrap(Student.updateOne({
              _id: payload._id
            }, payload));

          case 4:
            student = _context2.sent;
            res.json(student);
            _context2.next = 11;
            break;

          case 8:
            _context2.prev = 8;
            _context2.t0 = _context2["catch"](1);
            res.status(500).send(_context2.t0);

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  deleteStudent: function deleteStudent(req, res, next) {
    var id, student;
    return regeneratorRuntime.async(function deleteStudent$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            id = req.params.id; // res.json(studentController.deleteStudent(id))

            _context3.prev = 1;
            _context3.next = 4;
            return regeneratorRuntime.awrap(Student.deleteOne({
              _id: id
            }));

          case 4:
            student = _context3.sent;
            res.json(student);
            _context3.next = 11;
            break;

          case 8:
            _context3.prev = 8;
            _context3.t0 = _context3["catch"](1);
            res.status(500).send(_context3.t0);

          case 11:
          case "end":
            return _context3.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getStudents: function getStudents(req, res, next) {
    var students;
    return regeneratorRuntime.async(function getStudents$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            _context4.prev = 0;
            _context4.next = 3;
            return regeneratorRuntime.awrap(Student.find({}));

          case 3:
            students = _context4.sent;
            res.json(students);
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](0);
            res.status(500).send(_context4.t0);

          case 10:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[0, 7]]);
  },
  getStudent: function getStudent(req, res, next) {
    var id, student;
    return regeneratorRuntime.async(function getStudent$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            id = req.params.id;
            _context5.prev = 1;
            _context5.next = 4;
            return regeneratorRuntime.awrap(Student.find({
              _id: id
            }));

          case 4:
            student = _context5.sent;
            res.json(student);
            _context5.next = 11;
            break;

          case 8:
            _context5.prev = 8;
            _context5.t0 = _context5["catch"](1);
            res.status(500).send(_context5.t0);

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getStudentId: function getStudentId(req, res, next) {
    var id, student;
    return regeneratorRuntime.async(function getStudentId$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            id = req.params.id;
            _context6.prev = 1;
            _context6.next = 4;
            return regeneratorRuntime.awrap(Student.find({
              studentId: id
            }));

          case 4:
            student = _context6.sent;
            res.json(student);
            _context6.next = 11;
            break;

          case 8:
            _context6.prev = 8;
            _context6.t0 = _context6["catch"](1);
            res.status(500).send(_context6.t0);

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 8]]);
  }
};
module.exports = studentController;