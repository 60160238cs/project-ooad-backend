const User = require('../models/Educate.js')

const userController = {
  userList: [],
  lastId: 3,
  async addUser (req, res, next) {
    const payload = req.body
    const user = new User(payload)
    try {
      await user.save()
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUser (req, res, next) {
    const payload = req.body
    try {
      const user = await User.updateOne({ _id: payload._id }, payload)
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUser (req, res, next) {
    const { id } = req.params
    try {
      const users = await User.deleteOne({ _id: id })
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUsers (req, res, next) {
    try {
      const users = await User.find({})
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getUser (req, res, next) {
    const { id } = req.params
    try {
      const users = await User.findById(id)
      res.json(users)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  // List controller
  async addUserInList (req, res, next) {
    const { id } = req.params
    const payload = req.body
    console.log(payload)
    try {
      const user = await User.updateOne(
        { _id: id },
        { $push: { lists: payload } }
      )
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateUserInList (req, res, next) {
    const { id } = req.params
    const payload = req.body
    try {
      const user = await User.updateOne(
        { _id: id, 'lists._id': payload._id },
        {
          $set: {
            'lists.$.keyId': payload.keyId,
            'lists.$.lname': payload.lname,
            'lists.$.status': payload.status,
            'lists.$.checked': payload.checked
          }
        }
      )
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteUserInList (req, res, next) {
    const { id } = req.params
    const payload = req.body
    console.log(req.body)
    try {
      const user = await User.updateOne(
        { _id: id, 'lists._id': payload._id },
        {
          $pull: {
            lists: payload
          }
        }
      )
      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  //อันเดียวใช้ได้หมด ให้มองภาพเป็น document ก้อนเดียวจะทำง่ายกว่า
  async updateEducate (req, res, next) {
    console.log(req.body)
    try {
      const user = await User.updateOne({ _id: req.body._id}, req.body)

      res.json(user)
    } catch (err) {
      res.status(500).send(err)
    }
  },
}

module.exports = userController
