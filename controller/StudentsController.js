const Student = require('../models/Students')
const studentController = {
  studentList: [],
  lastId: 4,
  async addStudent (req, res, next) {
    const payload = req.body
    // res.json(studentController.addStudent(payload))
    const student = new Student(payload)
    try {
      await student.save()
      res.json(student)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateStudent (req, res, next) {
    const payload = req.body
    // res.json(studentController.updateStudent(payload))
    try {
      const student = await Student.updateOne({ _id: payload._id }, payload)
      res.json(student)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteStudent (req, res, next) {
    const { id } = req.params
    // res.json(studentController.deleteStudent(id))
    try {
      const student = await Student.deleteOne({ _id: id })
      res.json(student)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStudents (req, res, next) {
    // res.json(studentController.getStudents())
    try {
      const students = await Student.find({})
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStudent (req, res, next) {
    const { id } = req.params
    try {
      const student = await Student.find({ _id: id })
      res.json(student)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getStudentId (req, res, next) {
    const { id } = req.params
    try {
      const student = await Student.find({ studentId: id })
      res.json(student)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = studentController
