const Doc = require('../models/Doc')
const docsController = {
  documentList: [
    {
      ลำดับ: 1,
      ชื่อเอกสาร: 'ใบสมัครเป็นนิสิตสหกิจศึกษา',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S001.pdf'
    },
    {
      ลำดับ: 2,
      ชื่อเอกสาร: 'ใบสมัครงานสหกิจศึกษา/ฝึกงาน',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S002.pdf'
    },
    {
      ลำดับ: 3,
      ชื่อเอกสาร: 'แบบอนุญาตให้นิสิตไปปฏิบัติงานสหกิจศึกษา',
      ลิ้ง: 'https://www.informatics.buu.ac.th/coop/docs/nisit/IN-S003.pdf'
    }
  ],
  lastId: 4,
  async addDoc (req, res, next) {
    const payload = req.body
    delete payload._id

    const doc = new Doc(payload)
    try {
      await doc.save()
      res.json(doc)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async updateDoc (req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const doc = await Doc.updateOne({ _id: payload._id }, payload)
      res.json(doc)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteDoc (req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const doc = await Doc.deleteOne({ _id: id })
      res.json(doc)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDocs (req, res, next) {
    try {
      const docs = await Doc.find({})
      res.json(docs)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getDoc (req, res, next) {
    const { id } = req.params
    // res.json(usersController.getUser(id))
    // User.findById(id)
    //   .then(user => {
    //     res.json(user)
    //   })
    //   .catch(err => {
    //     res.status(500).send(err)
    //   })
    try {
      const doc = await Doc.findById(id)
      res.json(doc)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = docsController
