const dbHandler = require('./db-handler')
const Students = require('../models/Students')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const studentCooperativeStatusTrue = {
  studentId: '60160060',
  password: '12345678',
  name: 'ทศพร รัตนสำรวจ',
  faculty: 'วิทยาการสารสนเทศ',
  branch: 'CS',
  gpax: 3.52,
  cooperativeStatus: null,
  advisor: 'เบญจภรณ์ จันทรกองกุล',
  company: 'ClickNext',
  role: 'Frontend Developer',
  educateTime: {
    academicTime: 12,
    prepareTime: 30
  },
  registCooperativeStatus: false,
  subject: [
    {
      88510059: {
        name: '',
        grade: 4
      },
      88510259: {
        name: '',
        grade: 4
      },
      88510459: {
        name: '',
        grade: 4
      },
      88620159: {
        name: '',
        grade: 4
      },
      88620259: {
        name: '',
        grade: 4
      },
      88620359: {
        name: '',
        grade: 4
      },
      88620459: {
        name: '',
        grade: 4
      },
      88621159: {
        name: '',
        grade: 4
      },
      88634159: {
        name: '',
        grade: 4
      },
      88666659: {
        name: '',
        grade: 4
      }
    }
  ]
}

const studentErrorIdValidate = {
  studentId: '12345678',
  password: '12345678',
  name: 'ทศพร รัตนสำรวจ',
  faculty: 'วิทยาการสารสนเทศ',
  branch: 'CS',
  gpax: 3.52,
  cooperativeStatus: true,
  advisor: 'เบญจภรณ์ จันทรกองกุล',
  company: 'ClickNext',
  role: 'Frontend Developer',
  educateTime: {
    academicTime: 12,
    prepareTime: 30
  },
  registCooperativeStatus: false,
  subject: [
    {
      88510059: {
        name: '',
        grade: 4
      },
      88510259: {
        name: '',
        grade: 4
      },
      88510459: {
        name: '',
        grade: 4
      },
      88620159: {
        name: '',
        grade: 4
      },
      88620259: {
        name: '',
        grade: 4
      },
      88620359: {
        name: '',
        grade: 4
      },
      88620459: {
        name: '',
        grade: 4
      },
      88621159: {
        name: '',
        grade: 4
      },
      88634159: {
        name: '',
        grade: 4
      },
      88666659: {
        name: '',
        grade: 4
      }
    }
  ]
}

const studentErrorIdMin8 = {
  studentId: '6016',
  password: '12345678',
  name: 'ทศพร รัตนสำรวจ',
  faculty: 'วิทยาการสารสนเทศ',
  branch: 'CS',
  gpax: 3.52,
  cooperativeStatus: true,
  advisor: 'เบญจภรณ์ จันทรกองกุล',
  company: 'ClickNext',
  role: 'Frontend Developer',
  educateTime: {
    academicTime: 12,
    prepareTime: 30
  },
  registCooperativeStatus: false,
  subject: [
    {
      88510059: {
        name: '',
        grade: 4
      },
      88510259: {
        name: '',
        grade: 4
      },
      88510459: {
        name: '',
        grade: 4
      },
      88620159: {
        name: '',
        grade: 4
      },
      88620259: {
        name: '',
        grade: 4
      },
      88620359: {
        name: '',
        grade: 4
      },
      88620459: {
        name: '',
        grade: 4
      },
      88621159: {
        name: '',
        grade: 4
      },
      88634159: {
        name: '',
        grade: 4
      },
      88666659: {
        name: '',
        grade: 4
      }
    }
  ]
}

const studentErrorRoleEmpty = {
  studentId: '60160060',
  password: '12345678',
  name: 'ทศพร รัตนสำรวจ',
  faculty: 'วิทยาการสารสนเทศ',
  branch: 'CS',
  gpax: 3.52,
  cooperativeStatus: false,
  advisor: 'เบญจภรณ์ จันทรกองกุล',
  company: 'ClickNext',
  role: '',
  educateTime: {
    academicTime: 12,
    prepareTime: 30
  },
  registCooperativeStatus: false,
  subject: [
    {
      88510059: {
        name: '',
        grade: 4
      },
      88510259: {
        name: '',
        grade: 4
      },
      88510459: {
        name: '',
        grade: 4
      },
      88620159: {
        name: '',
        grade: 4
      },
      88620259: {
        name: '',
        grade: 4
      },
      88620359: {
        name: '',
        grade: 4
      },
      88620459: {
        name: '',
        grade: 4
      },
      88621159: {
        name: '',
        grade: 4
      },
      88634159: {
        name: '',
        grade: 4
      },
      88666659: {
        name: '',
        grade: 4
      }
    }
  ]
}

const studentComplete = {
  studentId: '60160060',
  password: '12345678',
  name: 'ทศพร รัตนสำรวจ',
  faculty: 'วิทยาการสารสนเทศ',
  branch: 'CS',
  gpax: 3.52,
  cooperativeStatus: false,
  advisor: 'เบญจภรณ์ จันทรกองกุล',
  company: 'ClickNext',
  role: 'Frontend Developer',
  educateTime: {
    academicTime: 12,
    prepareTime: 30
  },
  registCooperativeStatus: false,
  subject: [
    {
      88510059: {
        name: '',
        grade: 4
      },
      88510259: {
        name: '',
        grade: 4
      },
      88510459: {
        name: '',
        grade: 4
      },
      88620159: {
        name: '',
        grade: 4
      },
      88620259: {
        name: '',
        grade: 4
      },
      88620359: {
        name: '',
        grade: 4
      },
      88620459: {
        name: '',
        grade: 4
      },
      88621159: {
        name: '',
        grade: 4
      },
      88634159: {
        name: '',
        grade: 4
      },
      88666659: {
        name: '',
        grade: 4
      }
    }
  ]
}

describe('Students', () => {
  it('ไม่สามารถเพิ่มข้อมูลนิสิตที่มีชื่อในระบบ เพราะ กำลังฝึกสหกิจอยู่แล้ว', async () => {
    let error = null
    try {
      const student = new Students(studentCooperativeStatusTrue)
      await student.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มข้อมูลนิสิตที่ไม่มีชื่อในระบบ เพราะรหัสนิสิตผิด', async () => {
    let error = null
    try {
      const student = new Students(studentErrorIdValidate)
      await student.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มข้อมูลนิสิตได้ เพราะรหัสนิสิตไม่ครบ 8 ตัว', async () => {
    let error = null
    try {
      const student = new Students(studentErrorIdMin8)
      await student.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มข้อมูลนิสิตที่มีชื่อในระบบ และยังไม่เข้าฝึกสหกิจ เพราะตำแหน่งเว้นว่าง', async () => {
    let error = null
    try {
      const student = new Students(studentErrorRoleEmpty)
      await student.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('สามารถเพิ่มข้อมูลนิสิตที่มีชื่อในระบบ และยังไม่เข้าฝึกสหกิจได้', async () => {
    let error = null
    try {
      const student = new Students(studentComplete)
      await student.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
})
