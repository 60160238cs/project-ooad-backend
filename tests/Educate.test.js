const dbhandler = require('../tests/db-handler')
const Educate = require('../models/Educate')

beforeAll(async () => {
  await dbhandler.connect()
})

afterEach(async () => {
  await dbhandler.clearDatabase()
})

afterAll(async () => {
  await dbhandler.closeDatabase()
})
// ------------------------------------------------------สามารถเพิ่มการอบรมได้
const addEducateComplete = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', lname: 'Supawee', status: true, checked: false },
    { keyId: '60160272', lname: 'Sawee', status: true, checked: false },
    { keyId: '60160001', lname: 'Apin', status: true, checked: true }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้001
const addEducateIncomplete = {
  name: '',
  date: new Date(),
  maxsize: '',
  address: '',
  detail: '',
  lists: [],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้002
const addEducateIncomplete2 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: '',
  address: '',
  detail: '',
  lists: [],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้003
const addEducateIncomplete3 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: '',
  detail: '',
  lists: [],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้004
const addEducateIncomplete4 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: '',
  lists: [],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้005
const addEducateIncomplete5 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้006
const addEducateIncomplete6 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: ''
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้007
const addEducateIncomplete7 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: '' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้008
const addEducateIncomplete8 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้009
const addEducateIncomplete9 = {
  name: 'A',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้010
const addEducateIncomplete10 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 160,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้011
const addEducateIncomplete11 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 1,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้012
const addEducateIncomplete12 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: 1
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้013
const addEducateIncomplete13 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 123,
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้014
const addEducateIncomplete14 = {
  name: 789,
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้015
const addEducateIncomplete15 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 123123,
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้016
const addEducateIncomplete16 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 'aaa',
  address: 'Rayong',
  detail: 'No body no body',
  lists: [
    { keyId: '60160270', list_name: 'Supawee' },
    { keyId: '60160270', list_name: 'Supawee' }
  ],
  type: '1'
}
// ----------------------------------------------------------------------------------------------------------------------

describe('Educate', () => {
  it('สามารถเพิ่มการอบรมได้ ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateComplete)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ ท่านยังไม่ได้ใส่ข้อมูล', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก maxsize,address,detail,lists,type ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete2)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก address,detail,lists,type ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete3)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก detail,lists,type ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete4)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก lists,type ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete5)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก type ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete6)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก lists_name ใน lists ให้สมบูร ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete7)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก keyId ใน lists ให้สมบูร ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete8)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ name ต้องมาความยาวมากกว่า 2 ตัวอักษร ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete9)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize มากเกินไป ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete10)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize น้อยเกินไป ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete11)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize น้อยเกินไป ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete11)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ type ห้ามเป็นตัวเลข ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete12)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ address ห้ามเป็นตัวเลข ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete13)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ name ห้ามเป็นตัวเลข ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete14)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้ datail ห้ามเป็นตัวเลข ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete15)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
  it('ไม่สามารถเพิ่มการอบรมได้  maxsize ห้ามเป็นอักษร ', async () => {
    let error = null
    try {
      const educate = new Educate(addEducateIncomplete16)
      await educate.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})

// วิธีรัน    npx jest --runInBand ./tests
