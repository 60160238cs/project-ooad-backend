"use strict";

var dbhandler = require('../tests/db-handler');

var Educate = require('../models/Educate');

beforeAll(function _callee() {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(dbhandler.connect());

        case 2:
        case "end":
          return _context.stop();
      }
    }
  });
});
afterEach(function _callee2() {
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(dbhandler.clearDatabase());

        case 2:
        case "end":
          return _context2.stop();
      }
    }
  });
});
afterAll(function _callee3() {
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.next = 2;
          return regeneratorRuntime.awrap(dbhandler.closeDatabase());

        case 2:
        case "end":
          return _context3.stop();
      }
    }
  });
}); // ------------------------------------------------------สามารถเพิ่มการอบรมได้

var addEducateComplete = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    lname: 'Supawee',
    status: true,
    checked: false
  }, {
    keyId: '60160272',
    lname: 'Sawee',
    status: true,
    checked: false
  }, {
    keyId: '60160001',
    lname: 'Apin',
    status: true,
    checked: true
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้001

var addEducateIncomplete = {
  name: '',
  date: new Date(),
  maxsize: '',
  address: '',
  detail: '',
  lists: [],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้002

var addEducateIncomplete2 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: '',
  address: '',
  detail: '',
  lists: [],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้003

var addEducateIncomplete3 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: '',
  detail: '',
  lists: [],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้004

var addEducateIncomplete4 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: '',
  lists: [],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้005

var addEducateIncomplete5 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้006

var addEducateIncomplete6 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: ''
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้007

var addEducateIncomplete7 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: ''
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้008

var addEducateIncomplete8 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้009

var addEducateIncomplete9 = {
  name: 'A',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้010

var addEducateIncomplete10 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 160,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้011

var addEducateIncomplete11 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 1,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้012

var addEducateIncomplete12 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: 1
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้013

var addEducateIncomplete13 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 123,
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้014

var addEducateIncomplete14 = {
  name: 789,
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้015

var addEducateIncomplete15 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 150,
  address: 'Rayong',
  detail: 123123,
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ------------------------------------------------------ไม่สามารถเพิ่มการอบรมได้016

var addEducateIncomplete16 = {
  name: 'API Dev',
  date: new Date(),
  maxsize: 'aaa',
  address: 'Rayong',
  detail: 'No body no body',
  lists: [{
    keyId: '60160270',
    list_name: 'Supawee'
  }, {
    keyId: '60160270',
    list_name: 'Supawee'
  }],
  type: '1'
}; // ----------------------------------------------------------------------------------------------------------------------

describe('Educate', function () {
  it('สามารถเพิ่มการอบรมได้ ', function _callee4() {
    var error, educate;
    return regeneratorRuntime.async(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            error = null;
            _context4.prev = 1;
            educate = new Educate(addEducateComplete);
            _context4.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context4.next = 10;
            break;

          case 7:
            _context4.prev = 7;
            _context4.t0 = _context4["catch"](1);
            error = _context4.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ ท่านยังไม่ได้ใส่ข้อมูล', function _callee5() {
    var error, educate;
    return regeneratorRuntime.async(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            error = null;
            _context5.prev = 1;
            educate = new Educate(addEducateIncomplete);
            _context5.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context5.next = 10;
            break;

          case 7:
            _context5.prev = 7;
            _context5.t0 = _context5["catch"](1);
            error = _context5.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context5.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก maxsize,address,detail,lists,type ', function _callee6() {
    var error, educate;
    return regeneratorRuntime.async(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            error = null;
            _context6.prev = 1;
            educate = new Educate(addEducateIncomplete2);
            _context6.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context6.next = 10;
            break;

          case 7:
            _context6.prev = 7;
            _context6.t0 = _context6["catch"](1);
            error = _context6.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context6.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก address,detail,lists,type ', function _callee7() {
    var error, educate;
    return regeneratorRuntime.async(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            error = null;
            _context7.prev = 1;
            educate = new Educate(addEducateIncomplete3);
            _context7.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context7.next = 10;
            break;

          case 7:
            _context7.prev = 7;
            _context7.t0 = _context7["catch"](1);
            error = _context7.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context7.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก detail,lists,type ', function _callee8() {
    var error, educate;
    return regeneratorRuntime.async(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            error = null;
            _context8.prev = 1;
            educate = new Educate(addEducateIncomplete4);
            _context8.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context8.next = 10;
            break;

          case 7:
            _context8.prev = 7;
            _context8.t0 = _context8["catch"](1);
            error = _context8.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context8.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก lists,type ', function _callee9() {
    var error, educate;
    return regeneratorRuntime.async(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            error = null;
            _context9.prev = 1;
            educate = new Educate(addEducateIncomplete5);
            _context9.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context9.next = 10;
            break;

          case 7:
            _context9.prev = 7;
            _context9.t0 = _context9["catch"](1);
            error = _context9.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context9.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก type ', function _callee10() {
    var error, educate;
    return regeneratorRuntime.async(function _callee10$(_context10) {
      while (1) {
        switch (_context10.prev = _context10.next) {
          case 0:
            error = null;
            _context10.prev = 1;
            educate = new Educate(addEducateIncomplete6);
            _context10.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context10.next = 10;
            break;

          case 7:
            _context10.prev = 7;
            _context10.t0 = _context10["catch"](1);
            error = _context10.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context10.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก lists_name ใน lists ให้สมบูร ', function _callee11() {
    var error, educate;
    return regeneratorRuntime.async(function _callee11$(_context11) {
      while (1) {
        switch (_context11.prev = _context11.next) {
          case 0:
            error = null;
            _context11.prev = 1;
            educate = new Educate(addEducateIncomplete7);
            _context11.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context11.next = 10;
            break;

          case 7:
            _context11.prev = 7;
            _context11.t0 = _context11["catch"](1);
            error = _context11.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context11.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ กรุณากรอก keyId ใน lists ให้สมบูร ', function _callee12() {
    var error, educate;
    return regeneratorRuntime.async(function _callee12$(_context12) {
      while (1) {
        switch (_context12.prev = _context12.next) {
          case 0:
            error = null;
            _context12.prev = 1;
            educate = new Educate(addEducateIncomplete8);
            _context12.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context12.next = 10;
            break;

          case 7:
            _context12.prev = 7;
            _context12.t0 = _context12["catch"](1);
            error = _context12.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context12.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ name ต้องมาความยาวมากกว่า 2 ตัวอักษร ', function _callee13() {
    var error, educate;
    return regeneratorRuntime.async(function _callee13$(_context13) {
      while (1) {
        switch (_context13.prev = _context13.next) {
          case 0:
            error = null;
            _context13.prev = 1;
            educate = new Educate(addEducateIncomplete9);
            _context13.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context13.next = 10;
            break;

          case 7:
            _context13.prev = 7;
            _context13.t0 = _context13["catch"](1);
            error = _context13.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context13.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize มากเกินไป ', function _callee14() {
    var error, educate;
    return regeneratorRuntime.async(function _callee14$(_context14) {
      while (1) {
        switch (_context14.prev = _context14.next) {
          case 0:
            error = null;
            _context14.prev = 1;
            educate = new Educate(addEducateIncomplete10);
            _context14.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context14.next = 10;
            break;

          case 7:
            _context14.prev = 7;
            _context14.t0 = _context14["catch"](1);
            error = _context14.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context14.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize น้อยเกินไป ', function _callee15() {
    var error, educate;
    return regeneratorRuntime.async(function _callee15$(_context15) {
      while (1) {
        switch (_context15.prev = _context15.next) {
          case 0:
            error = null;
            _context15.prev = 1;
            educate = new Educate(addEducateIncomplete11);
            _context15.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context15.next = 10;
            break;

          case 7:
            _context15.prev = 7;
            _context15.t0 = _context15["catch"](1);
            error = _context15.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context15.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ maxsize น้อยเกินไป ', function _callee16() {
    var error, educate;
    return regeneratorRuntime.async(function _callee16$(_context16) {
      while (1) {
        switch (_context16.prev = _context16.next) {
          case 0:
            error = null;
            _context16.prev = 1;
            educate = new Educate(addEducateIncomplete11);
            _context16.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context16.next = 10;
            break;

          case 7:
            _context16.prev = 7;
            _context16.t0 = _context16["catch"](1);
            error = _context16.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context16.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ type ห้ามเป็นตัวเลข ', function _callee17() {
    var error, educate;
    return regeneratorRuntime.async(function _callee17$(_context17) {
      while (1) {
        switch (_context17.prev = _context17.next) {
          case 0:
            error = null;
            _context17.prev = 1;
            educate = new Educate(addEducateIncomplete12);
            _context17.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context17.next = 10;
            break;

          case 7:
            _context17.prev = 7;
            _context17.t0 = _context17["catch"](1);
            error = _context17.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context17.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ address ห้ามเป็นตัวเลข ', function _callee18() {
    var error, educate;
    return regeneratorRuntime.async(function _callee18$(_context18) {
      while (1) {
        switch (_context18.prev = _context18.next) {
          case 0:
            error = null;
            _context18.prev = 1;
            educate = new Educate(addEducateIncomplete13);
            _context18.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context18.next = 10;
            break;

          case 7:
            _context18.prev = 7;
            _context18.t0 = _context18["catch"](1);
            error = _context18.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context18.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ name ห้ามเป็นตัวเลข ', function _callee19() {
    var error, educate;
    return regeneratorRuntime.async(function _callee19$(_context19) {
      while (1) {
        switch (_context19.prev = _context19.next) {
          case 0:
            error = null;
            _context19.prev = 1;
            educate = new Educate(addEducateIncomplete14);
            _context19.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context19.next = 10;
            break;

          case 7:
            _context19.prev = 7;
            _context19.t0 = _context19["catch"](1);
            error = _context19.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context19.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้ datail ห้ามเป็นตัวเลข ', function _callee20() {
    var error, educate;
    return regeneratorRuntime.async(function _callee20$(_context20) {
      while (1) {
        switch (_context20.prev = _context20.next) {
          case 0:
            error = null;
            _context20.prev = 1;
            educate = new Educate(addEducateIncomplete15);
            _context20.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context20.next = 10;
            break;

          case 7:
            _context20.prev = 7;
            _context20.t0 = _context20["catch"](1);
            error = _context20.t0;

          case 10:
            expect(error).toBeNull();

          case 11:
          case "end":
            return _context20.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
  it('ไม่สามารถเพิ่มการอบรมได้  maxsize ห้ามเป็นอักษร ', function _callee21() {
    var error, educate;
    return regeneratorRuntime.async(function _callee21$(_context21) {
      while (1) {
        switch (_context21.prev = _context21.next) {
          case 0:
            error = null;
            _context21.prev = 1;
            educate = new Educate(addEducateIncomplete16);
            _context21.next = 5;
            return regeneratorRuntime.awrap(educate.save());

          case 5:
            _context21.next = 10;
            break;

          case 7:
            _context21.prev = 7;
            _context21.t0 = _context21["catch"](1);
            error = _context21.t0;

          case 10:
            expect(error).not.toBeNull();

          case 11:
          case "end":
            return _context21.stop();
        }
      }
    }, null, null, [[1, 7]]);
  });
}); // วิธีรัน    npx jest --runInBand ./tests